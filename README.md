# README #
Northwest wellness

# Team mates #
Hema Chaudary Paladugu (S530747)  
Swathi Dasari (S530667)  
Prem Kiran Osipalli (S530481)

### Purpose of the Northwest Wellness ###
To book an appointment, one does not need to go to wellness directly.   
This Northwest wellness application is helpful for users to book an appointment online.

### What is the repository for? ###
This is Northwest Wellness App  
Version: 0.0.1;  
SDK Level: APK 19  
Test Credentials: Our application need login functionality with Student mail and Password  
Supported Devices: Any android devices  
DataBase Used: Back4App DataBase  
Technologies Used: Java  
Tools Used: Andriod Studio, GitHub, BitBucket, Genymotion

### Sequence information ###
Firstly, The user needs to download the Northwest Wellness App.  
The welcome screen is the splash screen that contains a Home Button.  
When the user clicks on the home button, it takes to the second page that contains Make an appointment, Doctor Availability and cancel request.  
Third page is the Authentication page, which asks for student mail and password if the user choose the make make an appointment and cancel request.  
In the Make an Appointment page , the user has to enter first name, last name, Email ID, Phone Number, type of the health problem,Date and time.  
Once the user submit the application, user will get the conformation Email and the Text Message.  
The User can request for Appointment cancellation  by going through the cancel request.
User can see the doctors available at univwrsity wellness & give the feedback about the app.  
There is an Admin login which is used by the Admins with Authentication.   

========

1. App:  
User needs to download the application

2. Home Page:  
The home page has splash screen with home button
  
3. Basic Information:  
The basic information page contains 5 options 1. Make an Appointment 2. Doctors Availability 3. Cancel Request and 4. Admin Login 5.Feedback

4. Make an Appointment:  
This is an Authentication page with Student mail and password.The first time user should sign up with thier smail and a confirmation mail s sent to user from the database
and after verifying the smail user can login into the app. By clicking the submit button Details page will appear where one should provide his/her details
like  First Name, Last Name, Email-ID, Phone Number, Type of Health Problem, Date and Time. By submiting this page will lead you to Confirmation Page where 
Appointment details are displayed. By clicking on OK GOT IT button the Page will be directed to Basic Information Page.Once the Appointment is confirmed. 
If the user is logging and booking the appointment for the second time his/her details(name,mail,phonenumber) are automatically displayed in the personal information page
if the admin donot delete his/her first appointment.
 
5. Doctors Availability:  
In this page the doctors information those are available at the Northwest University Wellness Center are displayed along with the Wellness Office Hours and 
Emergency Contact Information

6. Cancel Request:  
In this page user is able to request for cancellation of his/her Appointment. Here he/she should again authenticate with Student mail and Password. 
By clicking the submit button user will get his/her previous Booked Appointments the user need to select the appointment that is to be requested and 
click Request for Cancel button. An alert Box will be displayed asking for confirmation to request for cancel. If we click Yes the request will be 
sent to admin for processing. If NO it will remain on the same page.

7.Feedback:
User can give feedback here and it will be updated in database and admin

8.Signup:
The first time user should signup here where he/she should provide his/her username,smail,password and clicking on the register button the 
user will be registered and a confirmation mail is sent to the users mail and if the user verified his/her mail he/she can loginto the app.

9. Admin Login:  
This page is for the Admin, this page displays an Authentication for Admin with the Admin ID and Password. 
Admin ID:admin
Password:admin
By Clicking the SIGNIN button.
It contains three options. 1.Booked Appointments and 2.Appointment Cancellations 3.Feedback from users
Booked Appointments consists of Appointments those are booked by the users and Appointment Cancellations consists of Cancellation Requests and if
 the admin clicks on anyone appointment cancellation request the appointment will be cancelled and the booked appointments are also updated

### How do I get set up?###

In order to download the project go through the following link https://s530481@bitbucket.org/s530481/northwest-wellness.git
and run the application in andriod studio.
Clone it to your local device and run the application.

Database:
we used the Back4app database because the data storage and retreival is fast and it is user friendly.
Database credentials:
Id:swathidasari754@gmail.com
Password:Swaram2018 

Already used user credentials for the app
User1
smail:S530667@nwmissouri.edu
Password:swaram
User2
smail:S530481@nwmissouri.edu
Password:prem

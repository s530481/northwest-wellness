package com.example.s530747.northwestwellness;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.parse.ParseInstallation;

public class MainActivity extends AppCompatActivity {

    Button home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        home=(Button)findViewById(R.id.btn_home);

    }
    public void onAction(View view){
        switch(view.getId()){
            case R.id.btn_home:
                Intent homeIntent= new Intent(this, Availability.class);
                startActivity(homeIntent);
                break;
        }
    }
}

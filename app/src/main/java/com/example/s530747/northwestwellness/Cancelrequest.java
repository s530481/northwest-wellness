package com.example.s530747.northwestwellness;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.s530747.northwestwellness.Model.PIDetails;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;

/**
 * Created by s530481 on 2/16/2018.
 */

public class Cancelrequest extends AppCompatActivity {
    Button btn_request;
    TextView tv_appoint_details1;
    String emailId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);
        setContentView(R.layout.activity_cancelrequest);
        btn_request = (Button) findViewById(R.id.btn_request);
        tv_appoint_details1 = (TextView) findViewById(R.id.tv_appoint_details1);
        emailId = getIntent().getStringExtra("emailId");
        Log.d("Emailid", "" + emailId);
        if (emailId != null && !emailId.equals("")) {
            getUserDetails(emailId);

        }

    }

    public void onAction(View view) {
        switch (view.getId()) {
            case R.id.btn_request:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Are you sure you want to cancel the appointment");
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                //     sendEmail("Cancel Appointment","Please Cancel the appointment for this "+emailId);
                                if (!emailId.equals("")) {
                                    updateAppointment(emailId, "cancel");
                                }
                                Toast.makeText(Cancelrequest.this, "Your request will be processed soon", Toast.LENGTH_LONG).show();
                                Intent end1 = new Intent(Cancelrequest.this, Authentication.class);
                                startActivity(end1);
                                finish();
                            }

                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Cancelrequest.this, Cancelrequest.class));
                        finish();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;

        }
    }

    public void getUserDetails(String emailId) {
        Log.d("Parse email id", emailId);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Back4AppAndroid");
        query.whereEqualTo("email", emailId.trim());
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> userList, ParseException e) {
                if (e == null) {
                    Log.d("Parse", "User retrieved:" + userList.size());
                    // Do something with the found objects
                    for (int i = 0; i < userList.size(); i++) {
                        String firstName = userList.get(i).get("firstname").toString();
                        String lastName = userList.get(i).get("lastname").toString();
                        String emailId = userList.get(i).get("email").toString();
                        String phoneNumber = userList.get(i).get("phonenumber").toString();
                        String date = userList.get(i).get("date").toString();
                        String time = userList.get(i).get("time").toString();
                        setValues(firstName, lastName, emailId, phoneNumber, date, time);

                    }
                } else {
                    Log.d("Parse", "Error: " + e.getMessage());
                }
            }
        });
    }


    public void updateAppointment(String emailId, String appointment) {
        ParseQuery query = ParseQuery.getQuery("Back4AppAndroid");
        query.whereEqualTo("email", emailId);
        try {
            ParseObject parseObject = query.getFirst();
            parseObject.put("appointment", "cancel");
            // parseObject.deleteInBackground();

            parseObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null)
                        Toast.makeText(Cancelrequest.this, "Your request will be processed soon", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(Cancelrequest.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (ParseException ex) {
            // Util.showToast(ex.getMessage().toString(),getApplicationContext());
        }
    }

    public void setValues(String firstName, String lastName, String emailId, String phoneNumber, String date, String time) {
        tv_appoint_details1.setText("First name   : " + firstName + " " + "Last Name   : "
                + lastName + "\n" + "Email ID   : " + emailId + "\n" + "Phone Number   : " + phoneNumber + "\n"
                + "Date of the Appointment   :" + date + "\n" + "Time of the Appointment   : " + time);
    }


    protected void sendEmail(String subject, String message) {
        String to = "swathidasari754@gmail.com";

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        //email.putExtra(Intent.EXTRA_CC, new String[]{ to});
        //email.putExtra(Intent.EXTRA_BCC, new String[]{to});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);

        //need this to prompts email client only
        email.setType("message/rfc822");

        startActivityForResult(Intent.createChooser(email, "hemachoudary@gmail.com"), 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        startActivity(new Intent(Cancelrequest.this, Availability.class));
        finish();
    }

}


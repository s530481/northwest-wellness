package com.example.s530747.northwestwellness;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ListView;

/**
 * Created by S530747 on 2/16/2018.
 */

public class Doctorlist extends AppCompatActivity {
    ListView lst;
    String [] doctorname ={"University Wellness Services","Dr. Gerald Wilmes, MD","Linda Guess","BK Taylor","Dr. Mike Mattock","Evan Rand"};
    String [] docdesc ={"660.562.1348,Office Hours: 8am-5pm, Monday through Friday","Executive/Medical Director","Medical Office Manager","Assistant Director, WS-Prevention, Outreach, and Education","Counselor","Assistant Director"};
    Integer [] imagid ={R.drawable.wellness,R.drawable.md,R.drawable.linda,R.drawable.taylor,R.drawable.mike,R.drawable.evan};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctorslist);
        lst =(ListView)findViewById(R.id.listview);
        Customlistview customlistview=new Customlistview(this,doctorname,docdesc,imagid);
        lst.setAdapter(customlistview);
    }
}

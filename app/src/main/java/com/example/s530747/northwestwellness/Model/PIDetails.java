package com.example.s530747.northwestwellness.Model;

/**
 * Created by S530667 on 3/8/2018.
 */

public class PIDetails {
    private static String id;
    private static String firstName;
    private static String lastName;
    private static String emailId;
    private static String phoneCode;
    private static String phoneNumber;
    private static String diseaseType;

    private static String date;
    private static String time;
    private static String password;

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        PIDetails.password = password;
    }

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        PIDetails.id = id;
    }

    public static String getFirstName() {
        return firstName;
    }

    public static void setFirstName(String firstName) {
        PIDetails.firstName = firstName;
    }

    public static String getLastName() {
        return lastName;
    }

    public static void setLastName(String lastName) {
        PIDetails.lastName = lastName;
    }

    public static String getEmailId() {
        return emailId;
    }

    public static void setEmailId(String emailId) {
        PIDetails.emailId = emailId;
    }

    public static String getPhoneCode() {
        return phoneCode;
    }

    public static void setPhoneCode(String phoneCode) {
        PIDetails.phoneCode = phoneCode;
    }

    public static String getPhoneNumber() {
        return phoneNumber;
    }

    public static void setPhoneNumber(String phoneNumber) {
        PIDetails.phoneNumber = phoneNumber;
    }

    public static String getDiseaseType() {
        return diseaseType;
    }

    public static void setDiseaseType(String diseaseType) {
        PIDetails.diseaseType = diseaseType;
    }

    public static String getDate() {
        return date;
    }

    public static void setDate(String date) {
        PIDetails.date = date;
    }

    public static String getTime() {
        return time;
    }

    public static void setTime(String time) {
        PIDetails.time = time;
    }
}




package com.example.s530747.northwestwellness;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

public class Authentication extends AppCompatActivity {
    Button btn_submit;
    Button btn_back;
    EditText et_num919;
    EditText et_password;
    TextView tv_please;
    Button btn_signup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);
        setContentView(R.layout.activity_authentication);
        btn_submit= (Button)findViewById(R.id.btn_submit);
        btn_back=(Button)findViewById(R.id.btn_back);
        et_num919=(EditText)findViewById(R.id.et_num919);
        et_password=(EditText)findViewById(R.id.et_password);
        // tv_please=(TextView)findViewById(R.id.tv_please);
        btn_signup=(Button)findViewById(R.id.btn_signup);




    }
    public void onAction(View view){
        switch (view.getId()){
            case R.id.btn_submit:
                String id=et_num919.getText().toString().trim();
                String pass=et_password.getText().toString().trim();
                checkUser(id,pass);
               /* if(id.equals("919560579")&&(pass.equals("swathi"))||id.equals("919559567")|| (pass.equals("hema"))||(pass.equals("prem"))||id.equals("919559290")){
                    Intent submit=new Intent(this,Personalinformation.class);
                    startActivity(submit);
                }
                else{
                    tv_please.setText("please enter the details");

                }*/
                break;
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_signup:
                Intent end =new Intent(this, Signup.class);
                startActivity(end);
                finish();
                break;



        }
    }

    public void moveToPersonalInformation(String emailId){
        Intent piIntent=new Intent(Authentication.this,Personalinformation.class);
        piIntent.putExtra("emailId",emailId);
        startActivity(piIntent);
        finish();
    }

    public void checkUser(final String emailId, String password){

        ParseUser.logInInBackground(emailId, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (parseUser != null) {
                    if(parseUser.getBoolean("emailVerified")) {
                        moveToPersonalInformation(emailId);
                    }
                    else
                    {
                        ParseUser.logOut();
                        Toast.makeText(Authentication.this,"Please Verify Your Email first",Toast.LENGTH_SHORT).show();

                    }
                } else {
                    ParseUser.logOut();
                    Toast.makeText(Authentication.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });

    /*    ParseQuery<ParseUser> query=ParseUser.getQuery();
        query.whereEqualTo("email",emailId);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> parseUsers, ParseException e) {
                if(e == null){
                    if(parseUsers.size()>0){
                        Toast.makeText(Authentication.this,"User already Exist", Toast.LENGTH_SHORT).show();
                        moveToPersonalInformation(emailId);
                    }else{
                        Toast.makeText(Authentication.this, "Please Sign up", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(Authentication.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });*/

    }
}

package com.example.s530747.northwestwellness;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by S530747 on 2/16/2018.
 */

public class Adminlogin extends AppCompatActivity {
    Button btn_signin;
    EditText id;
    EditText pwd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminlogin);
        btn_signin=(Button)findViewById(R.id.btn_signin);
        id=(EditText)findViewById(R.id.et_adminid);
        pwd=(EditText)findViewById(R.id.et_pwd);

    }
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signin:
                String idcheck=id.getText().toString();
                String idpwd=id.getText().toString();
                String a="admin";
                String b="admin";
                if(idcheck.equals(a)&&idpwd.equals(b)) {
                    Intent signin = new Intent(this, Appointments.class);
                    startActivity(signin);
                    finish();
                }
                else{
                    Toast.makeText(this,"Please check the credentials",Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.btn_back:

                Intent back = new Intent(this, Availability.class);
                startActivity(back);
                finish();
                break;
        }
    }
}

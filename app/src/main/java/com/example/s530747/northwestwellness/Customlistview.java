package com.example.s530747.northwestwellness;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by s530481 on 3/9/2018.
 */

public class Customlistview extends ArrayAdapter<String> {

    private String[] doctorname;
    private String [] docdesc;
    private Integer [] imagid;
    private Activity context;
    public Customlistview(Activity context, String[] doctorname, String [] docdesc, Integer[] imagid) {
        super(context, R.layout.listview_layout,doctorname);
        this.context=context;
        this.doctorname=doctorname;
        this.docdesc=docdesc;
        this.imagid=imagid;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View r = convertView;
        ViewHolder viewHolder =null;
        if(r==null){
            LayoutInflater layoutInflater=context.getLayoutInflater();
            r=layoutInflater.inflate(R.layout.listview_layout,null,true);
            viewHolder=new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else{
            viewHolder=(ViewHolder) r.getTag();

        }
        viewHolder.img.setImageResource((imagid[position]));
        viewHolder.tx1.setText(doctorname[position]);
        viewHolder.tx2.setText(docdesc[position]);
        return r;
    }
    class  ViewHolder{
        TextView tx1;
        TextView tx2;
        ImageView img;
        ViewHolder(View v){
            tx1=(TextView) v.findViewById(R.id.docname);
            tx2 =(TextView) v.findViewById(R.id.docdesc);
            img=(ImageView) v.findViewById(R.id.image);
        }
    }
}

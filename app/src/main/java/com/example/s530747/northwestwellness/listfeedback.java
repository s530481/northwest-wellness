package com.example.s530747.northwestwellness;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class listfeedback extends AppCompatActivity {
    Button btn_feedback;
    ListView list_feed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);
        setContentView(R.layout.activity_listfeedback);
        list_feed=(ListView)findViewById(R.id.list_feed);
        btn_feedback=(Button)findViewById(R.id.btn_feedback);
        btn_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("FeedBack");
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> userList, ParseException e) {
                        if (e == null) {
                            Log.d("Parse", "User retrieved:" + userList.size());

                            ArrayList<String> arrayjob = new ArrayList<>();

                            for (ParseObject j : userList) {
                                String userDetails = j.get("feedback").toString();
                                arrayjob.add(userDetails);
                            }
                            ArrayAdapter adapter = new ArrayAdapter(listfeedback.this, android.R.layout.simple_list_item_1, arrayjob);
                            list_feed.setAdapter(adapter);
                        } else {
                            Toast.makeText(listfeedback.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
//    }
    }


    }


package com.example.s530747.northwestwellness;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.parse.ParseObject;

public class Signup extends AppCompatActivity {
    EditText et_pwd;
    EditText et_cfpwd;
    EditText et_919;
    TextView tv_919;
    TextView tv_pwd;
    TextView tv_cfpwd;
    Button btn_register;
    EditText et_user;
    Button cancel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);

        setContentView(R.layout.activity_signup);
        et_919 = (EditText) findViewById(R.id.et_919);
        et_pwd = (EditText) findViewById(R.id.ed_pwd);
        et_cfpwd = (EditText) findViewById(R.id.ed_cfpwd);
        tv_919 = (TextView) findViewById(R.id.tv_919);
        tv_pwd = (TextView) findViewById(R.id.tv_pwd);
        tv_cfpwd = (TextView) findViewById(R.id.tv_cfpwd);
        btn_register =(Button)findViewById(R.id.btn_register);
        et_user=(EditText)findViewById(R.id.et_user);
        cancel=(Button)findViewById(R.id.btn_cancelregister);


    }

    public void onAction(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                String num = et_919.getText().toString();
                String pwd = et_pwd.getText().toString();
                String cfpwd =et_cfpwd.getText().toString();
                String user=et_user.getText().toString();
                if(num!=null && !num.isEmpty() && pwd!=null && !pwd.isEmpty() && cfpwd!=null && !cfpwd.isEmpty() && pwd.equals(cfpwd)){
                    registerUser(num,pwd,num);
                    /*Intent register=new Intent(this,Authentication.class);
                    startActivity(register);*/
                }
                else{
                    Toast.makeText(this,"Please check the credentials",Toast.LENGTH_LONG).show();
                }
            case R.id.btn_cancelregister:
                Intent cancel=new Intent(this,Authentication.class);
                startActivity(cancel);
                finish();

        }
    }


    private void alertDisplayer(String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(Signup.this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        // don't forget to change the line below with the names of your Activities
                        Toast.makeText(Signup.this, "please verify email before login", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Signup.this,Authentication.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                });
        AlertDialog ok = builder.create();
        ok.show();
    }

    public void registerUser(final String emailID, String password,String userName){

        final ParseUser user = new ParseUser();
// Set the user's username and password, which can be obtained by a forms
        user.setUsername(userName);
        user.setEmail(emailID);
        user.setPassword(password);
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    ParseUser.logOut();
                    alertDisplayer("Account Created Successfully!", "Please verify your email before Login");
                } else {
                    ParseUser.logOut();
                    Toast.makeText(Signup.this, e.getMessage(), Toast.LENGTH_LONG).show();


                }
            }
        });
       /* ParseUser.logInInBackground(userName, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (parseUser != null) {
                    alertDisplayer("Sucessful Login","Welcome back" + userName + "!");

                } else {
                    ParseUser.logOut();
                    Toast.makeText(Signup.this, "Invalid credentials", Toast.LENGTH_LONG).show();
                }
            }
        });*/
    }
}



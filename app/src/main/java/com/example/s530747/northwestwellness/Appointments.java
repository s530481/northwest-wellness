package com.example.s530747.northwestwellness;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;


/**
 * Created by S530747 on 2/16/2018.
 */

public class Appointments extends AppCompatActivity {
    Button btn_logout;
    Button btn_booked_appointments;
    Button feed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_appointments);
        btn_logout=(Button)findViewById(R.id.btn_logout);
        btn_booked_appointments=(Button)findViewById(R.id.btn_booked_appointments);
        feed=(Button)findViewById(R.id.btn_feed);

    }
    public void onPress(View view) {
        switch (view.getId()) {
            case R.id.btn_logout:
                Intent logout = new Intent(this, Adminlogin.class);
                startActivity(logout);
                finish();
                break;
            case R.id.btn_cancel:
                Intent cancel = new Intent(this, CancelAppointment.class);
                startActivity(cancel);

                break;
            case R.id.btn_booked_appointments:
                Intent booked= new Intent(this, BookedAppointments.class);
                startActivity(booked);
                break;
            case R.id.btn_feed:
                Intent feed=new Intent(this,listfeedback.class);
                startActivity(feed);


        }
    }

}

package com.example.s530747.northwestwellness;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.s530747.northwestwellness.Model.PIDetails;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;


/**
 * Created by s530481 on 2/16/2018.
 */

public class Confirmation extends AppCompatActivity {

    Button btn_done;

    TextView tv_appoint_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);
        setContentView(R.layout.activity_confirmation);
        btn_done = (Button) findViewById(R.id.btn_done);
        tv_appoint_details = (TextView) findViewById(R.id.tv_appoint_details);


           /* Bundle bundle=getIntent().getBundleExtra("PIBundle");
        tv_appoint_details.setText(bundle.getString("firstname","") +" "+bundle.getString("lastname",""));*/
        tv_appoint_details.setText(PIDetails.getFirstName() + " " + PIDetails.getLastName() + "\n " + PIDetails.getEmailId() + "\n"
                + PIDetails.getPhoneCode() + " " + PIDetails.getPhoneNumber() + "\n " + PIDetails.getDiseaseType() + " \n" + PIDetails.getDate() + " \n" + PIDetails.getTime());

        String email=PIDetails.getEmailId();
    }

    public void onAction(View view) {
        switch (view.getId()) {
            case R.id.tv_appoint_details:
                Intent conf = new Intent(this, Personalinformation.class);
                startActivityForResult(conf, 101);


            case R.id.btn_done:

                Intent end = new Intent(this, Availability.class);
                startActivity(end);

                break;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 101) {
            String firstname = data.getStringExtra("firstname");
            tv_appoint_details.setText("" + firstname);


        }
    }


}



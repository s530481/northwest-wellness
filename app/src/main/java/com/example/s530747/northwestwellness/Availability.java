package com.example.s530747.northwestwellness;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Availability extends AppCompatActivity {
    ImageButton btn_appointment;
    ImageButton btn_doctor_availability;
    ImageButton btn_cancel_request;
    ImageButton btn_adminlogin;
    ImageButton feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_availability);
        btn_appointment=(ImageButton) findViewById(R.id.btn_appointment);
        btn_doctor_availability=(ImageButton) findViewById(R.id.btn_doctor_availability);
        btn_cancel_request=(ImageButton) findViewById(R.id.btn_cancel_request);
        btn_adminlogin=(ImageButton) findViewById(R.id.btn_adminlogin);
        feedback=(ImageButton) findViewById(R.id.btn_feedback);

    }
    public void onAction(View view){
        switch (view.getId()){
            case R.id.btn_appointment:
                Intent appointment= new Intent (this, Authentication.class);
                startActivity(appointment);

                break;
            case R.id.btn_adminlogin:
                Intent login= new Intent (this,Adminlogin.class);
                startActivity(login);

                break;
            case R.id.btn_cancel_request:
                Intent cancel= new Intent (this, Cancelauthentication.class);
                startActivity(cancel);

                break;
            case R.id.btn_doctor_availability:
                Intent list= new Intent (this, Doctorlist.class);
                startActivity(list);

                break;
        }
    }
    public void onClick(View v){
        Intent feedback= new Intent (this, Feedback.class);
        startActivity(feedback);
    }
}

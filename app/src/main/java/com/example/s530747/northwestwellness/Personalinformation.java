package com.example.s530747.northwestwellness;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.List;

import com.example.s530747.northwestwellness.Model.PIDetails;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.Calendar;

import okhttp3.internal.Util;


public class Personalinformation extends AppCompatActivity {
    Spinner dropdown;
    EditText et_first;
    EditText et_last;
    EditText et_email;
    // EditText et_phone_code;
    EditText et_phone_number;
    Spinner spinner1;
    Button btn_submit;
    Button pickdate;
    Button picktime;
    Button cancela;

    ParseObject parseObject;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    String[] diseases;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);
        setContentView(R.layout.activity_personalinformation);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        cancela=(Button)findViewById(R.id.btn_cancel);
        pickdate = (Button) findViewById(R.id.btn_date);
        picktime = (Button) findViewById(R.id.tv_time);
        et_first = (EditText) findViewById(R.id.et_first);
        et_last = (EditText) findViewById(R.id.et_last);
        et_email = (EditText) findViewById(R.id.et_email);
        diseases=getResources().getStringArray(R.array.health_problem_list);

        //  et_phone_code=(EditText)findViewById(R.id.et_phone_code);
        et_phone_number = (EditText) findViewById(R.id.et_phone_number);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        String emailId = getIntent().getStringExtra("emailId");
        Log.d("Emailid", "" + emailId);
        if (emailId != null && !emailId.equals("")) {
            getUserDetails(emailId);

        }
    }

    public void onAction(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:


                String firstname = et_first.getText().toString().trim();
                String lastname = et_last.getText().toString().trim();
                String email = et_email.getText().toString().trim();
                String et_phone_numbers = et_phone_number.getText().toString().trim();
                String date=pickdate.getText().toString().trim();
                String time=picktime.getText().toString().trim();
                String disease_type=spinner1.getSelectedItem().toString();
                Log.d("disease_type",disease_type);
                //    String et_phone_codes=et_phone_code.getText().toString().trim();
                if (firstname.equals("")) {
                    et_first.setError("Please Enter First Name");
                } else if (lastname.equals("")) {
                    et_last.setError("Please Enter Last Name");
                } else if (email.equals("")) {
                    et_email.setError("please Enter the Email ID");
                } else if (et_phone_numbers.equals("")) {
                    et_phone_number.setError("please Enter the phone number");
                } else {
                    PIDetails.setFirstName(firstname);
                    PIDetails.setLastName(lastname);
                    PIDetails.setEmailId(email);
                    // PIDetails.setPhoneCode(et_phone_codes);
                    PIDetails.setPhoneNumber(et_phone_numbers);
                    PIDetails.setDiseaseType(spinner1.getSelectedItem().toString());
                    Log.d("Date", pickdate.getText().toString());
                    PIDetails.setDate(pickdate.getText().toString());
                    Log.d("Date", PIDetails.getDate());
                    Log.d("Time", picktime.getText().toString());
                    PIDetails.setTime(picktime.getText().toString());
                    Log.d("Time", PIDetails.getTime());
                    String to = PIDetails.getEmailId();
                    // String to="swathidasari754@gmail.com";
                    String subject = "University Wellness Services on";
                    String message = "Appointment Details " + "\n  " + PIDetails.getFirstName() + " " + PIDetails.getLastName() + "\n"
                            + PIDetails.getPhoneCode() + " " + PIDetails.getPhoneNumber() + "\n " + PIDetails.getDiseaseType() +
                            " \n" + PIDetails.getDate() + " \n" + PIDetails.getTime() + "\n  " + "If YOU NO LONGER NEED THIS APPOINTMENT, " + "\n " +
                            "PLEASE CANCEL IN A TIMELY MANNER SO WE MAY OFFER THIS APPOINTMENT TO OTHER STUDENTS IN NEED. " + "\n  "
                            + "You may cancel online through the Wellness Portal(http://wellness.nwmissouri.edu) or by calling wellness services at 660-562-1348." + "\n  "
                            + "You may leave a message after hours.";


                    parseObject = new ParseObject("Back4AppAndroid");
                    parseObject.put("firstname", firstname);
                    parseObject.put("lastname", lastname);
                    parseObject.put("email", email);
                    parseObject.put("phonenumber", et_phone_numbers);
                    parseObject.put("date",date);
                    parseObject.put("time",time);
                    parseObject.put("disease",disease_type);
                    parseObject.put("appointment","new");
                    parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            Toast.makeText(Personalinformation.this,"Saved Succesfully",Toast.LENGTH_SHORT).show();
                            Intent end = new Intent(Personalinformation.this, Confirmation.class);
                            startActivity(end);
                            finish();

                        }
                    });

                    //  parseObject.put("text",et_phone_numbers);
                    // parseObject.saveInBackground();

                    //   parseObject.put("text",pickdate);
                    //   parseObject.put("text",picktime);

                    //editText.setText("");
                    // updateOrDelete.setText("");
//                        parseObject.saveInBackground(new SaveCallback() {
//                            @Override
//                            public void done(ParseException e) {
//                                if (e == null)
//
//                                    Util.showToast("Saved on server Successfully",getApplicationContext());
//                                else
//                                    Util.showToast(e.getMessage().toString(),getApplicationContext());
//                            }
//                        });
//
//
//                        Util.showToast("Enter a text to send.", getApplicationContext());

//
//                    Intent emailconf = new Intent(Intent.ACTION_SEND);
//                    emailconf.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
//                    emailconf.putExtra(Intent.EXTRA_SUBJECT, subject);
//                    emailconf.putExtra(Intent.EXTRA_TEXT, message);
//                    emailconf.setType("message/rfc822");
//                    startActivity(Intent.createChooser(emailconf, "Choose an Email client :"));
//                    sendEmail(subject,message);





                }
                break;
            case R.id.btn_date:
                //Tooltip tooltip =new Tooltip.Builder(pickdate).setText("hai").show();


                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(Personalinformation.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        pickdate.setText((month + 1) + "/" + dayOfMonth + "/" + year);
                    }
                }, year, month, day);
                datePickerDialog.show();
//                break;

                final AlertDialog dialog=new AlertDialog.Builder(this).setTitle("WELLNESS Office Days")
                        .setMessage("Open from MONDAY to FRIDAY")
                        .setPositiveButton("OK",null).show();
                Button Psoitive=dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                Psoitive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(Personalinformation.this, "Now select the date", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
                break;
            case R.id.tv_time:
                //Tooltip tooltip1 =new Tooltip.Builder(picktime).setText("hai").show();
                Calendar calendar1 = Calendar.getInstance();
                int hour = calendar1.get(Calendar.HOUR);
                int minute = calendar1.get(Calendar.MINUTE);
                timePickerDialog = new TimePickerDialog(Personalinformation.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String AM_PM;
                        if (hourOfDay < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }
                        picktime.setText(hourOfDay + ":" + minute + " " + AM_PM);
                    }
                }, hour, minute, false);
                timePickerDialog.show();
                final AlertDialog dialog1=new AlertDialog.Builder(this).setTitle("WELLNESS Office Hours")
                        .setMessage("Open from 8:00AM to 5:00 PM")
                        .setPositiveButton("OK",null).show();
                Button Positive=dialog1.getButton(AlertDialog.BUTTON_POSITIVE);
                Positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(Personalinformation.this, "Now select the time", Toast.LENGTH_LONG).show();
                        dialog1.dismiss();
                    }
                });
                break;
            case R.id.btn_cancel:
                Intent cancela=new Intent(this,Authentication.class);
                startActivity(cancela);
                finish();
        }


    }


    protected void sendEmail(String subject, String message) {
        String to = PIDetails.getEmailId().trim();

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        //email.putExtra(Intent.EXTRA_CC, new String[]{ to});
        //email.putExtra(Intent.EXTRA_BCC, new String[]{to});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);

        //need this to prompts email client only
        email.setType("message/rfc822");

        startActivityForResult(Intent.createChooser(email, "hemachoudary@gmail.com"), 100);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent end = new Intent(this, Confirmation.class);
        startActivity(end);
        finish();
    }

    public void getUserDetails(String emailId) {
        Log.d("Parse email id", emailId);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Back4AppAndroid");
        query.whereEqualTo("email", emailId.trim());
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> userList, ParseException e) {
                if (e == null) {
                    Log.d("Parse", "User retrieved:" + userList.size());
                    // Do something with the found objects
                    for (int i = 0; i < userList.size(); i++) {
                        String firstName = userList.get(i).get("firstname").toString();
                        String lastName = userList.get(i).get("lastname").toString();
                        String emailId = userList.get(i).get("email").toString();
                        String phoneNumber =  userList.get(i).get("phonenumber").toString();
                        String date=userList.get(i).get("date").toString();
                        String time=userList.get(i).get("time").toString();
                        String disease_type="";
                        if(userList.get(i).get("disease")!=null){
                            disease_type=userList.get(i).get("disease").toString();
                        }
                        setValues(firstName, lastName, emailId, phoneNumber,date,time,disease_type);


                    }
                } else {
                    Log.d("Parse", "Error: " + e.getMessage());
                }
            }
        });
    }

/*
                ParseQuery query = ParseQuery.getQuery("Back4AppAndroid");
                query.whereEqualTo("emailID", emailId);
                try {

                    parseObject.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject object, ParseException e) {
                            if (e == null) {
                                if (object != null) {
                                    String firstName = object.getString("firstname");
                                    String lastName = object.getString("lastname");
                                    String emailId = object.getString("emailID");
                                    String phoneNumber = object.getString("phonenumber");
                                    setValues(firstName, lastName, emailId, phoneNumber);
                                }
                            }
                        }
                    });
                } catch (ParseException e) {
                    e.printStackTrace();
                }*/


//}
private void alertDisplayer(String title,String message){
    AlertDialog.Builder builder = new AlertDialog.Builder(Personalinformation.this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    // don't forget to change the line below with the names of your Activities

                    Intent intent = new Intent(Personalinformation.this,Authentication.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
    AlertDialog ok = builder.create();
    ok.show();
}


    public void setValues(String firstName, String lastName, String emailId, String phoneNumber,String date,String time,String disease_type) {
        et_first.setText(firstName);
        et_last.setText(lastName);
        et_email.setText(emailId);
        et_phone_number.setText(phoneNumber);
        pickdate.setText(date);
        picktime.setText(time);
        if(!disease_type.equals("")){
            for(int i=0;i<diseases.length;i++){
                if(diseases[i].equals(disease_type.trim())){
                    spinner1.setSelection(i);
                    break;
                }
            }

        }


    }



}

package com.example.s530747.northwestwellness;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.s530747.northwestwellness.Model.PIDetails;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class BookedAppointments extends AppCompatActivity {
    TextView tv_booked;
    String list = "";
    Button btn_booked;
    ListView listbook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);
        setContentView(R.layout.activity_booked_appointments);
        listbook = (ListView) findViewById(R.id.listbook);
        //  TextView tv_booked = (TextView) findViewById(R.id.tv_booked);
        btn_booked = (Button) findViewById(R.id.btn_booked);
        btn_booked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Back4AppAndroid");
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> userList, ParseException e) {
                        if (e == null) {
                            Log.d("Parse", "User retrieved:" + userList.size());

                            ArrayList<String> arrayjob = new ArrayList<>();

                            for (ParseObject j : userList) {
                                String userDetails = j.get("firstname").toString()+" "+j.get("lastname").toString()+"\n"+j.get("email").toString()+j.get("phonenumber");
                                arrayjob.add(userDetails);
                            }
                            ArrayAdapter adapter = new ArrayAdapter(BookedAppointments.this, android.R.layout.simple_list_item_1, arrayjob);
                            listbook.setAdapter(adapter);
                        } else {
                            Toast.makeText(BookedAppointments.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
//    }
    }
}


//getAllData();


//    public void getUserDetails(String emailId) {
//        Log.d("Parse email id", emailId);
//        ParseQuery<ParseObject> query = ParseQuery.getQuery("Back4AppAndroid");
//        query.findInBackground(new FindCallback<ParseObject>() {
//            public void done(List<ParseObject> userList, ParseException e) {
//                if (e == null) {
//                    Log.d("Parse", "User retrieved:" + userList.size());
//                    // Do something with the found objects
//                    for (int i = 0; i < userList.size(); i++) {
//                        String firstName = userList.get(i).get("firstname").toString();
//                        String lastName = userList.get(i).get("lastname").toString();
//                        String emailId = userList.get(i).get("email").toString();
//                        String phoneNumber =  userList.get(i).get("phonenumber").toString();
//                        setValues(firstName, lastName, emailId, phoneNumber);
//
//                    }
//                } else {
//                    Log.d("Parse", "Error: " + e.getMessage());
//                }
//            }
//        });
//    }
//    public List<ParseObject> getAllData() {
//        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Back4AppAndroid");
//        query.findInBackground(new FindCallback<ParseObject>() {
//            @Override
//            public void done(List<ParseObject> userList, ParseException e) {
//                if (e == null) {
//                    Log.d("Parse", "User retrieved:" + userList.size());
//                    // Do something with the found objects
//                    for (int i = 0; i < userList.size(); i++) {
//                        String firstName = userList.get(i).get("firstname").toString();
//                        String lastName = userList.get(i).get("lastname").toString();
//                        String emailId = userList.get(i).get("email").toString();
//                        String phoneNumber = userList.get(i).get("phonenumber").toString();
//                        setValues(firstName, lastName, emailId, phoneNumber);
//
//                    }
//                } else {
//                    Log.d("Parse", "Error: " + e.getMessage());
//                }
//            }
//
//        });
//       return getAllData();
//    }
//
//
//
//    public void setValues(String firstName,String lastName,String emailId,String phoneNumber){
//        tv_booked.setText(firstName +" "+lastName+"\n "+emailId+"\n"
//                +phoneNumber);
//    }
//    public List<ParseObject> getAllData() {
//        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("User");
//        query.findInBackground(new FindCallback<ParseObject>() {
//            @Override
//            public void done(List<ParseObject> users,ParseException e) {
//                if (e == null) {
//                    list = users;
//                }
//            }
//        }); return list;
//    }
//}

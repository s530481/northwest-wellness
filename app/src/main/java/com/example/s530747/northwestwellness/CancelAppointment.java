package com.example.s530747.northwestwellness;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.s530747.northwestwellness.Model.PIDetails;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class CancelAppointment extends AppCompatActivity {
   /* EditText et_email;
    TextView tv_cancel_details;*/

    ListView listView;
    List<UserDetails> piDetailsList;
    ArrayAdapter adapter;
    ArrayList<String> arrayjob;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);
        setContentView(R.layout.cancel_appointment);
        piDetailsList=new ArrayList<>();
        listView=(ListView)findViewById(R.id.cancel_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(!piDetailsList.isEmpty()){
                    UserDetails userDetails=piDetailsList.get(position);
                    deleteInformation(userDetails.email,position);
                   /* PIDetails piDetails=new PIDetails();
                    piDetails=piDetailsList.get(position).;
                    deleteInformation(piDetails.);*/
                }
            }
        });
        getUserDetails("cancel");

       /* et_email=(EditText)findViewById(R.id.et_email);
        tv_cancel_details=(TextView)findViewById(R.id.tv_cancel_details);*/

    }

  /*  public void onAction(View view){
        switch(view.getId()){
            case R.id.btn_fetch:
                getUserDetails(et_email.getText().toString());
                break;
            case R.id.btn_cancel:
                deleteInformation(et_email.getText().toString());
                break;
        }

    }
*/



    public void getUserDetails(String appointment) {
        Log.d("Parse email id", appointment);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Back4AppAndroid");
        query.whereEqualTo("appointment",appointment);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> userList, ParseException e) {
                if (e == null) {
                    Log.d("Parse", "User retrieved:" + userList.size());
                    // Do something with the found objects

                    arrayjob = new ArrayList<>();
                    for (int i = 0; i < userList.size(); i++) {
                        String firstName = userList.get(i).get("firstname").toString();
                        String lastName = userList.get(i).get("lastname").toString();
                        String emailId = userList.get(i).get("email").toString();
                        String phoneNumber =  userList.get(i).get("phonenumber").toString();
                        String date=userList.get(i).get("date").toString();
                        String time=userList.get(i).get("time").toString();
                         UserDetails userDetails=new UserDetails(firstName,lastName,emailId);
                        piDetailsList.add(userDetails);
                        String useDetails=firstName+" "+lastName+"\n"+emailId;
                        arrayjob.add(useDetails);


                     //   setValues(firstName, lastName, emailId, phoneNumber,date,time);


                    }

                    ArrayAdapter adapter = new ArrayAdapter(CancelAppointment.this, android.R.layout.simple_list_item_1, arrayjob);
                    listView.setAdapter(adapter);
                } else {
                    Log.d("Parse", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void setValues(String firstName, String lastName, String emailId, String phoneNumber,String date,String time) {
       // tv_cancel_details.setText("First name   : "+firstName+" "+"Last Name   : "+lastName+"\n"+"Email ID   : "+emailId+"\n"+"Phone Number   : "+phoneNumber+"\n"+"Date of the Appointment  : "+date+"\n"+"Time of the Appointment  : "+time);
    }


    public void deleteInformation(String emailId, final int position){
        ParseObject parseObject;
        ParseQuery query = ParseQuery.getQuery("Back4AppAndroid");
        query.whereEqualTo("email", emailId);
        try {
            parseObject = query.getFirst();


        parseObject.deleteInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(CancelAppointment.this, "The Appointment is Canceled. Feel free to book an appointment again", Toast.LENGTH_SHORT).show();
                    piDetailsList.remove(position);
                    clearAdapter(position);
                   // finish();
                }
                else
                    Toast.makeText(CancelAppointment.this,e.getMessage(),Toast.LENGTH_SHORT).show();
            }

        });
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void clearAdapter(int position){
        if(adapter!=null && !adapter.isEmpty()){
            if(arrayjob!=null && !arrayjob.isEmpty()){
                arrayjob.remove(position);
                adapter.notifyDataSetChanged();
            }
        }
    }

    class UserDetails {
        private String firstName="";
        private String lastName="";
        private String email="";
        public UserDetails(String firstName,String lastName,String email){
            this.firstName=firstName;
            this.lastName=lastName;
            this.email=email;
        }

    }
}
